﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AirSeaAndSun2._0.Models;

namespace AirSeaAndSun2._0.Controllers
{
    public class HebergementsController : Controller
    {
        private AirSeaSunEntities2 db = new AirSeaSunEntities2();

        // GET: Hebergements
        public ActionResult Index()
        {
            var hebergements = db.Hebergements.Include(h => h.Adresse).Include(h => h.Client);
            return View(hebergements.ToList());
        }

        // GET: Hebergements/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hebergement hebergement = db.Hebergements.Find(id);
            if (hebergement == null)
            {
                return HttpNotFound();
            }
            return View(hebergement);
        }

        // GET: Hebergements/Create
        public ActionResult Create()
        {
            ViewBag.IdAdresse = new SelectList(db.Adresses, "IdAdresse", "Numero");
            ViewBag.IdClient = new SelectList(db.Clients, "IdClient", "Nom");
            return View();
        }

        // POST: Hebergements/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdHebergement,IdAdresse,IdClient,NomHebergement,Photo,TypeHebergement,Description,PrixDeBase,Etat,Departement")] Hebergement hebergement)
        {
            if (ModelState.IsValid)
            {
                db.Hebergements.Add(hebergement);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdAdresse = new SelectList(db.Adresses, "IdAdresse", "Numero", hebergement.IdAdresse);
            ViewBag.IdClient = new SelectList(db.Clients, "IdClient", "Nom", hebergement.IdClient);
            return View(hebergement);
        }

        // GET: Hebergements/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hebergement hebergement = db.Hebergements.Find(id);
            if (hebergement == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdAdresse = new SelectList(db.Adresses, "IdAdresse", "Numero", hebergement.IdAdresse);
            ViewBag.IdClient = new SelectList(db.Clients, "IdClient", "Nom", hebergement.IdClient);
            return View(hebergement);
        }

        // POST: Hebergements/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdHebergement,IdAdresse,IdClient,NomHebergement,Photo,TypeHebergement,Description,PrixDeBase,Etat,Departement")] Hebergement hebergement)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hebergement).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdAdresse = new SelectList(db.Adresses, "IdAdresse", "Numero", hebergement.IdAdresse);
            ViewBag.IdClient = new SelectList(db.Clients, "IdClient", "Nom", hebergement.IdClient);
            return View(hebergement);
        }

        // GET: Hebergements/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hebergement hebergement = db.Hebergements.Find(id);
            if (hebergement == null)
            {
                return HttpNotFound();
            }
            return View(hebergement);
        }

        // POST: Hebergements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Hebergement hebergement = db.Hebergements.Find(id);
            db.Hebergements.Remove(hebergement);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
