﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AirSeaAndSun2._0.Models;

namespace AirSeaAndSun2._0.Controllers
{
    public class FavorisController : Controller
    {
        private AirSeaSunEntities2 db = new AirSeaSunEntities2();

        // GET: Favoris
        public ActionResult Index()
        {
            var favoris = db.Favoris.Include(f => f.Client).Include(f => f.Hebergement);
            return View(favoris.ToList());
        }

        // GET: Favoris/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Favori favori = db.Favoris.Find(id);
            if (favori == null)
            {
                return HttpNotFound();
            }
            return View(favori);
        }

        // GET: Favoris/Create
        public ActionResult Create()
        {
            ViewBag.IdClient = new SelectList(db.Clients, "IdClient", "Nom");
            ViewBag.IdHebergement = new SelectList(db.Hebergements, "IdHebergement", "NomHebergement");
            return View();
        }

        // POST: Favoris/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdClient,IdHebergement,Commentaire")] Favori favori)
        {
            if (ModelState.IsValid)
            {
                db.Favoris.Add(favori);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdClient = new SelectList(db.Clients, "IdClient", "Nom", favori.IdClient);
            ViewBag.IdHebergement = new SelectList(db.Hebergements, "IdHebergement", "NomHebergement", favori.IdHebergement);
            return View(favori);
        }

        // GET: Favoris/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Favori favori = db.Favoris.Find(id);
            if (favori == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdClient = new SelectList(db.Clients, "IdClient", "Nom", favori.IdClient);
            ViewBag.IdHebergement = new SelectList(db.Hebergements, "IdHebergement", "NomHebergement", favori.IdHebergement);
            return View(favori);
        }

        // POST: Favoris/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdClient,IdHebergement,Commentaire")] Favori favori)
        {
            if (ModelState.IsValid)
            {
                db.Entry(favori).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdClient = new SelectList(db.Clients, "IdClient", "Nom", favori.IdClient);
            ViewBag.IdHebergement = new SelectList(db.Hebergements, "IdHebergement", "NomHebergement", favori.IdHebergement);
            return View(favori);
        }

        // GET: Favoris/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Favori favori = db.Favoris.Find(id);
            if (favori == null)
            {
                return HttpNotFound();
            }
            return View(favori);
        }

        // POST: Favoris/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Favori favori = db.Favoris.Find(id);
            db.Favoris.Remove(favori);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
